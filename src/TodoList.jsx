import InputMessage from './InputMessage';
import List from './List';
import { useState } from 'react';
function TodoList(){
    
    const [todoList,setTodoList] = useState([]);
    const addTodo=(todoObj)=>{
        const todos=todoList;
        const newTodos=[...todos,todoObj];
        setTodoList(newTodos);
    }
    
    return(
        <div className='todoList'>
            <List todos={todoList}/>
            <InputMessage addTodo={addTodo}/>
        </div>
    )
}
export default TodoList;