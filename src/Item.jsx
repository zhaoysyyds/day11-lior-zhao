import './Item.css'

function Item({message}){
    
    
    return(
        <div className='item'>
            {message}
        </div>
    )
}
export default Item;