import './InputMessage.css';
import {useRef} from 'react';

const InputMessage= ({addTodo})=>{
    const messageRef=useRef();

    const addMessage=()=>{
        const message=messageRef.current.value;
        if(message===''){
            alert('输入为空');
        }
        else{
            addTodo(message);
        }
        messageRef.current.value='';
    }
    
    return(
        <div className='inputMessage'>
            <input ref={messageRef}></input>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <button onClick={addMessage}>add</button>
        </div>
    )
}
export default InputMessage;