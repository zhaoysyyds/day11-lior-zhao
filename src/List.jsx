import Item from "./Item";
import './List.css'
function List({todos}){
    const messageList=[...todos];
    return(
        <div className='list'>
            {messageList.map((value,index)=>(<Item key={index} message={value}/>))}
        </div>
    )
}
export default List;